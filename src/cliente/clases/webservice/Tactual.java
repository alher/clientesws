package cliente.clases.webservice;

public class Tactual {
	private String temp;
	private String ph;
	private String hora;
	private String fecha;
	
	public Tactual(String temp, String ph, String hora, String fecha){
		
		this.temp = temp;
		this.ph = ph;
		this.hora = hora;
		this.fecha = fecha; 
	}
	
	public String gettemp(){
		return temp;
	}
	
	public String getph(){
		return ph;
	}
	
	public String gethora(){
		return hora;
	}
	
	public String getfecha(){
		return fecha;
	}
	
}
