package cliente.clases.webservice;

public class Sensor {
	private String id;
	private String nombre;
	
	public Sensor(String id, String nombre){
		this.id = id;
		this.nombre = nombre;
	}
	
	public String getid(){
		return id;
	}
	
	public String getnombre(){
		return nombre;
	}
}
