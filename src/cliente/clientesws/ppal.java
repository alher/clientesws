package cliente.clientesws;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.content.res.Configuration;



public class ppal extends ActionBarActivity {
	
	private String [] menus;
	private DrawerLayout navdra;
	private ListView lista;
	private ActionBarDrawerToggle drawerToggle;
	
	private CharSequence tituloSeccion;
	private CharSequence tituloApp;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ppal);
				
		menus = new String [] {"Temperatura Actual","Rango de Temperaturas"};
		navdra = (DrawerLayout) findViewById(R.id.drawer_ppal);
		lista = (ListView) findViewById(R.id.menu_drawer);
		
		lista.setAdapter (new ArrayAdapter<String> (getSupportActionBar().getThemedContext(),
				 		android.R.layout.simple_list_item_activated_1, menus));
		 
		lista.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				Fragment fragment = null;
				
				switch(position){
					case 0:
						fragment = new fragment_tactual();
					break;
					case 1:
						fragment = new fragment_rtemp();
					break;
				
				}
				FragmentManager fman = getSupportFragmentManager();
				
				fman.beginTransaction()
				.replace(R.id.contenido, fragment)
				.commit();

		        lista.setItemChecked(position, true);

		        tituloSeccion = menus[position];
		        getSupportActionBar().setTitle(tituloSeccion);

		        navdra.closeDrawer(lista);
		     		
			}
			 
		});
		
		tituloSeccion = getTitle();
		tituloApp = getTitle();

		drawerToggle = new ActionBarDrawerToggle(this, 
				navdra,
				R.drawable.ic_navigation_drawer, 
				R.string.drawer_open,
				R.string.drawer_close) {

			public void onDrawerClosed(View view) {
				getSupportActionBar().setTitle(tituloSeccion);
				ActivityCompat.invalidateOptionsMenu(ppal.this);
			}

			public void onDrawerOpened(View drawerView) {
				getSupportActionBar().setTitle(tituloApp);
				ActivityCompat.invalidateOptionsMenu(ppal.this);
			}
		};

		navdra.setDrawerListener(drawerToggle);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		if (drawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		
		switch(item.getItemId()){
			case R.id.cerrar_sesion:
				Toast.makeText(ppal.this, "Sesión Terminada", Toast.LENGTH_SHORT).show();
				gotohome();
			break;	
		}
		
		return true;
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		drawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		drawerToggle.onConfigurationChanged(newConfig);
	}
	
	public void gotohome(){
		Intent nueva_act = new Intent (ppal.this, Login.class);
		startActivity(nueva_act);
		finish();
	}

}
