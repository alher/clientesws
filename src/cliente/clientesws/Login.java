package cliente.clientesws;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;

public class Login extends Activity {

	EditText user, pass;
	TextView tv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		user = (EditText) findViewById(R.id.user);
		pass = (EditText) findViewById(R.id.pass);
		tv = (TextView)findViewById(R.id.textView1);
		
		Button entrar = (Button) findViewById(R.id.entrar);
		entrar.setOnClickListener(new OnClickListener() {
				
			@Override
			public void onClick(View v) {
				Servicio_login serv = new Servicio_login();
				serv.execute();
			}
		});
		
	}
	
	private class Servicio_login extends AsyncTask<String,Integer,Boolean> {

		
		String res;
		
		@Override
		protected Boolean doInBackground(String... params) {
			
			Boolean resul = true; 
			
			final String metodo = "login";
		    final String namespace = "http://tempuri.org/";
			final String url = "http://192.168.0.118:8095/Service1.asmx";
			final String accionsoap = "http://tempuri.org/login";
			
			try{
				SoapObject request = new SoapObject(namespace, metodo);
				request.addProperty("usu", user.getText().toString());
				request.addProperty("cont", pass.getText().toString());
				
				SoapSerializationEnvelope sobre = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				sobre.dotNet = true; 
				sobre.setOutputSoapObject(request);
				
				HttpTransportSE transporte = new HttpTransportSE(url);
				
				transporte.call(accionsoap , sobre);
				
				SoapPrimitive resultado = (SoapPrimitive)sobre.getResponse();
				
				res = resultado.toString();
				
			}catch(Exception e){
				res = e.getMessage();
				resul = false;
			}	
			return resul;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			
			tv.setText(res);
			
			if(get_res().equals("y")){	
				Toast.makeText(Login.this, "Identificación exitosa", Toast.LENGTH_SHORT).show();
				gotonextactivity();
			}else{
				Toast.makeText(Login.this, "Identificación fallida", Toast.LENGTH_SHORT).show();
			}
		}
		
		public String get_res(){
			return res;
		}
		
		public void gotonextactivity(){	
			Intent nueva_act = new Intent (Login.this , ppal.class);
			startActivity(nueva_act);
		}
		
	}

}