package cliente.clientesws;

import java.util.Timer;
import java.util.TimerTask;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import cliente.clases.webservice.Acuario;
import cliente.clases.webservice.Sensor;
import cliente.clases.webservice.Tactual;
import android.widget.AdapterView.OnItemSelectedListener;

public class fragment_tactual extends Fragment{	
	
	Servicio_acuario sa;
	Servicio_sensor ss;
	Servicio_tactual st;
	
	Spinner acuarios;
	Spinner sensores;
	
	TextView tiempo;
	
	EditText temp;
	EditText ph;
	EditText hora;
	EditText fecha;
	
	
	String [] ad_acuario;
	String [] ad_sensor;
	
	Acuario [] acua;
	Sensor [] sens;
	Tactual [] t_act;
	
	String id_acuario;
	String id_sensor;
	
	int time;
	Timer t;
	TimerTask task;
	Handler handler; 
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootview = inflater.inflate(R.layout.fragment_tactual, container, false);
		acuarios = (Spinner)rootview.findViewById(R.id.acuario);
		sensores = (Spinner)rootview.findViewById(R.id.sensor);
		
		tiempo = (TextView)rootview.findViewById(R.id.timer);
		
		temp = (EditText)rootview.findViewById(R.id.temp);
		ph = (EditText)rootview.findViewById(R.id.ph);
		hora = (EditText)rootview.findViewById(R.id.hora);
		fecha = (EditText)rootview.findViewById(R.id.fecha);
		
		
		sa = new Servicio_acuario();
		sa.execute();	
		
		acuarios.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				
				id_acuario = acua[position].getid();
				ss = new Servicio_sensor();
				ss.execute();
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		sensores.setOnItemSelectedListener(new OnItemSelectedListener() {
			
			public void onItemSelected(AdapterView<?> parent, View view, int position,
					long id) {
				
				id_sensor = sens[position].getid();
				System.out.println("id_sensor: " + id_sensor + "\nid_acuario: "+ id_acuario);
				
				//st = new Servicio_tactual();
				//st.execute();
						
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		startTimer();	
		
		return rootview;
		
	}
	
	public void startTimer() {
		time = 5;
		t = new Timer();
		handler = new Handler();
		task = new TimerTask() {
			// this method is called every 1ms
			public void run() {
				handler.post(new Runnable() {
					public void run() {
						tiempo.setText("" + time);
						
						System.out.println("id_acuario: " + id_acuario + "id_sensor:" + id_sensor);
						
						st = new Servicio_tactual();
						st.execute();
						
						if (time > 0) {
							time -= 1;
						} else {
							t.purge();
							t.cancel();
							startTimer();
						}
					}
				});
				
			}
		};
		t.schedule(task, 0, 1000);
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
	}
	
	public String [] getad_acuario(Acuario [] acua){
		
		String [] a = new String[acua.length];
		
		for(int i=0; i<a.length; i++){
			a[i] = acua[i].getid();
		}
		return a;
	}
	
	public String [] getad_sensor(Sensor [] sen){
		
		String [] a = new String[sen.length];
		
		for(int i=0; i<a.length; i++){
			a[i] = sen[i].getnombre();
		}
		return a;
	}
	
	private class Servicio_acuario extends AsyncTask<String,Integer,Boolean> {
	
		@Override
		protected Boolean doInBackground(String... params) {
			
			Boolean resul = true; 
			
			final String metodo = "arreglo_acuarios";
		    final String namespace = "http://tempuri.org/";
			final String url = "http://192.168.0.118:8095/Service1.asmx";
			final String accionsoap = "http://tempuri.org/arreglo_acuarios";
			
			try{
				SoapObject request = new SoapObject(namespace, metodo);
				
				SoapSerializationEnvelope sobre = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				sobre.dotNet = true; 
				sobre.setOutputSoapObject(request);
				
				HttpTransportSE transporte = new HttpTransportSE(url);
				
				transporte.call(accionsoap , sobre);				
				
				SoapObject resultado = (SoapObject)sobre.getResponse();
				
				acua = new Acuario[resultado.getPropertyCount()];
				
				for (int i = 0; i<acua.length; i++){
					
					SoapObject ic = (SoapObject)resultado.getProperty(i);
					Acuario acu = new Acuario(ic.getProperty(0).toString());
					acua[i] = acu;
				}
						
			}catch(Exception e){
				resul = false;
			}	
			return resul;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			
			if(result){
				
				ArrayAdapter<String> adaptador =
				        new ArrayAdapter<String>(fragment_tactual.this.getActivity(),
				            android.R.layout.simple_spinner_item, getad_acuario(acua));
				acuarios.setAdapter(adaptador);
				 	
			}else{
				
			}
			
		}
			
	}

	private class Servicio_sensor extends AsyncTask<String,Integer,Boolean> {
	
		@Override
		protected Boolean doInBackground(String... params) {
			
			Boolean resul = true; 
			
			final String metodo = "arreglo_sen_acu";
		    final String namespace = "http://tempuri.org/";
			final String url = "http://192.168.0.118:8095/Service1.asmx";
			final String accionsoap = "http://tempuri.org/arreglo_sen_acu";
			
			try{
				SoapObject request = new SoapObject(namespace, metodo);
				request.addProperty("id_acuario", id_acuario);
				
				SoapSerializationEnvelope sobre = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				sobre.dotNet = true; 
				sobre.setOutputSoapObject(request);
				
				HttpTransportSE transporte = new HttpTransportSE(url);
				
				transporte.call(accionsoap , sobre);				
				
				SoapObject resultado = (SoapObject)sobre.getResponse();
				
				sens = new Sensor[resultado.getPropertyCount()];
				
				for (int i = 0; i<sens.length; i++){
					
					SoapObject ic = (SoapObject)resultado.getProperty(i);
					Sensor sen = new Sensor(ic.getProperty(0).toString(), ic.getProperty(1).toString());
					sens[i] = sen;
					
				}
						
			}catch(Exception e){
				resul = false;
			}	
			return resul;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
				
			ArrayAdapter<String> adaptador =
					new ArrayAdapter<String>(fragment_tactual.this.getActivity(),
				            android.R.layout.simple_spinner_item, getad_sensor(sens));
			sensores.setAdapter(adaptador);


		}
	}
	
	private class Servicio_tactual extends AsyncTask<String,Integer,Boolean> {
		
		//Tactual t_ac [];
		
		String t,p,h,f;
		
		@Override
		protected Boolean doInBackground(String... params) {
			
			Boolean resul = true; 
			
			final String metodo = "arreglo_tactual";
		    final String namespace = "http://tempuri.org/";
			final String url = "http://192.168.0.118:8095/Service1.asmx";
			final String accionsoap = "http://tempuri.org/arreglo_tactual";
			
			try{
				SoapObject request = new SoapObject(namespace, metodo);
				request.addProperty("id_acuario", id_acuario);
				request.addProperty("id_sensor", id_sensor);
				
				SoapSerializationEnvelope sobre = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				sobre.dotNet = true; 
				sobre.setOutputSoapObject(request);
				
				HttpTransportSE transporte = new HttpTransportSE(url);
				
				transporte.call(accionsoap , sobre);				
				
				SoapObject resultado = (SoapObject)sobre.getResponse();
				
				
				//t_act = new Tactual[resultado.getPropertyCount()];
				
				SoapObject ic = (SoapObject)resultado.getProperty(0);
				
				System.out.println("Trayendo temp: " + ic.getProperty(0).toString());
				System.out.println("Trayendo ph: " + ic.getProperty(1).toString());
				System.out.println("Trayendo hora: " + ic.getProperty(3).toString());
				System.out.println("Trayendo fecha: " + ic.getProperty(2).toString());
				
				t = ic.getProperty(0).toString();
				p = ic.getProperty(1).toString();
				h = ic.getProperty(3).toString();
				f = ic.getProperty(2).toString();
				
				
		
				/*for (int i = 0; i<t_act.length; i++){
					
					//SoapObject ic = (SoapObject)resultado.getProperty(i);
					Tactual tac = new Tactual(ic.getProperty(0).toString(), ic.getProperty(1).toString(),
												ic.getProperty(3).toString(), ic.getProperty(2).toString());
					
					System.out.println("Consumiendo el servicio: \n"+ "Posicion:"+ i +"\n" +
							"Temp:"+ ic.getProperty(0).toString() + "\n" + 
							"ph:"+ ic.getProperty(1).toString() + "\n" +
							"Hora:"+ ic.getProperty(2).toString() +"\n" +
							"Fecha:" + ic.getProperty(3).toString());
																
					t_act[i] = tac;
					System.out.println("Ya en el objeto: \n"+ "Posicion:"+ i +"\n" +
						    "Temp:"+ t_ac[i].gettemp()+ "\n" + 
						    "ph:"+ t_ac[i].getph() + "\n" +
							"Hora:"+ t_ac[i].gethora()+"\n" +
							"Fecha:" + t_ac[i].getfecha());
					
				}*/
						
			}catch(Exception e){
				resul = false;
			}	
			return resul;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			
			temp.setText("" + t);
			ph.setText("" +  p);
			hora.setText("" + h);
			fecha.setText("" + f);
						
		}
	}

	
}
