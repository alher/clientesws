package cliente.soporte.controles.seleccion;

public class Titulos {
	private String titulo;
	private String subtitulo;
	
	public Titulos(String titulo){
		this.titulo = titulo;
	}
	
	public Titulos(String titulo, String subtitulo){
		this.titulo = titulo;
		this.subtitulo = subtitulo;
	}
	
	public String gettitulo(){
		return titulo;
	}
	
	public String getsubtitulo(){
		return subtitulo;
	}
}
